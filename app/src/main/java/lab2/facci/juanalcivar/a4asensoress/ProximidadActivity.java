package lab2.facci.juanalcivar.a4asensoress;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ProximidadActivity extends AppCompatActivity implements SensorEventListener {

    Sensor sensorProximidad;
    SensorManager sensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proximidad);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorProximidad = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensorProximidad, SensorManager.SENSOR_DELAY_FASTEST);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.values[0] < sensorProximidad.getMaximumRange()) {
            getWindow().getDecorView().setBackgroundColor(Color.GREEN);
        }else{
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
